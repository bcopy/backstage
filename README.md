# Backstage Test repository

## How to deploy via Helm

```bash
helm repo add janus-idp-backstage https://janus-idp.github.io/helm-backstage
helm repo add paas-helm-charts https://registry.cern.ch/chartrepo/paas-helm-charts
helm repo update

helm install backstage -f values.yaml janus-idp-backstage/backstage
helm install cern-auth-proxy -f cern-auth-proxy.values.yaml paas-helm-charts/cern-auth-proxy
```

## How to update the Backstage configuration

### Prerequisites

* You will need a JDK v11+
* (Alternatively you must have node v16+ and yarn 1.0+ installed)

* You can use the provided Gradle wrapper to install node locally (Alternatively you must have node v14+ installed)
  * ```./gradlew installNode installYarn```
* You can then setup your path and install the required dependencies :
  * ```source ./setup-path.sh```
  * ```cd accsoft-backstage && yarn install && yarn dev```


